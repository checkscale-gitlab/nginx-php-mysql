# Nginx, PHP and MySQL

A compose set of containers providing Nginx, php (php-fpm) and MySQL (MariaDB).

Also bundled in here is mailcatcher so you can capture mail sent from your applications whilst in development.

## Usage

Place your php/html application under `./html` and start the containers.

```shell
$ docker-compose up -d
```

Access the application at http://localhost:8080

### Nginx

You can modify the configuration of Nginx using the file `conf.d/default.conf` or add your own configuration file. Just make sure you mount the same volumes onto the `php-fpm` service so it can be seen in the same place on both services.

Remove the defaul `./html/index.php` file or overwrite it.

### PHP

You can add to the PHP configuration by modifying the `php.ini` file. This gets included as `/usr/local/etc/php/conf.d/docker-php-custom.ini` so is additive to the default PHP config.

The one included just demonstrates a simple change of an unused (in Linux) parameter `SMTP = mail` instead of the default `SMTP = localhost`.

### MySQL

This is provided by MariaDB.

### Mailcatcher

Access mailcatcher at http://localhost:1080